package com.jira.environment;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import com.jira.error.ServiceFailureException;

public class Env {

	private static Env inst;
	private Properties prop = new Properties();
	private final String BASE_DIR;

	private Env() {
		this.BASE_DIR = "./";
		try {
			this.prop.load(new FileInputStream(BASE_DIR + "/env.prop"));
		} catch (Exception e) {
			throw new ServiceFailureException(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Environment Init Failed!!",
					e.getCause());
		}
	}

	public static void init() {
		inst = new Env();
	}

	public static String baseDir() {
		return Env.inst.BASE_DIR;
	}
	
	public static String jiraURL() {
		return Env.reqStrProp("JIRA_BASE_URL");
	}

	public static String prop(String propName) {
		return Env.inst.prop.getProperty(propName);
	}

	public static String prop(String propName, String defaultValue) {
		return Env.inst.prop.getProperty(propName, defaultValue);
	}

	public static Integer intProp(String propName, Integer defaultVal) {
		String val = prop(propName);
		if (val == null) {
			return defaultVal;
		}
		return Integer.parseInt(val);
	}

	public static String reqStrProp(String propName) {
		String p = Env.inst.prop.getProperty(propName);
		assert(p != null);
		return p;
	}

	public static boolean isDev() {
		String env = Env.inst.prop.getProperty("environment", "prod");
		return env.equalsIgnoreCase("dev");
	}

}
