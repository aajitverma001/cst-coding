package com.jira.error;

public class ServiceFailureException extends RuntimeException {

	public int httpCode;
	public String message;

	public ServiceFailureException(int httpCode, String message) {
		this(httpCode, message, null);
	}

	public ServiceFailureException(int httpCode, String message, Throwable th) {
		super(String.format("[%s] - %s", httpCode, message), th);
		this.httpCode = httpCode;
		this.message = message;
	}
}
