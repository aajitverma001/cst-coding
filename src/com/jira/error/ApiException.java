package com.jira.error;

import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

public class ApiException extends ServiceFailureException {

	public ApiException(int httpCode, String message) {
		this(httpCode, message, null);
	}

	public ApiException(int httpCode, String message, Throwable th) {
		super(httpCode, message, th);
	}

	public JSONObject asJSON() {
		JSONObject err = new JSONObject();
		err.put("http_code", httpCode);
		err.put("message", message);
		return err;
	}

	public static ApiException wrap(Exception ex) {
		return new ApiException(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
				"Sorry, Something went wrong. " + "Please retry after some time.", ex.getCause());
	}
}
