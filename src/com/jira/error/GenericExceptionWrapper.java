package com.jira.error;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

public class GenericExceptionWrapper implements ExceptionMapper<Throwable> {

	@Override
	public Response toResponse(Throwable exception) {
		Exception e = new Exception(exception);
		ApiException ex = ApiException.wrap(e);
		return Response.status(ex.httpCode).entity(ex.asJSON()).type(MediaType.APPLICATION_JSON).build();
	}

}
