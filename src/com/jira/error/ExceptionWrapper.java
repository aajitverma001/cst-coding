package com.jira.error;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

public class ExceptionWrapper implements ExceptionMapper<ApiException> {

	@Override
	public Response toResponse(ApiException ex) {
		return Response.status(ex.httpCode).entity(ex.asJSON()).type(MediaType.APPLICATION_JSON).build();
	}

}
