package com.jira.sqs;

import com.sqs.client.intf.AwsQueueClient;
import com.sqs.codec.impl.SimpleMessageCodec;
import static com.sqs.client.initializer.AwsQueueInitializer.*;

/**
 * The queues can be defined here.
 * 
 * @author cb-ajit
 *
 */
public interface QueueDefn {

	AwsQueueClient.GeneralQueue<SimpleMessageCodec.Encoder, SimpleMessageCodec.Decoder> JIRA_POINT_SUM = instQ(
			"jira-point-sum", SimpleMessageCodec.TYPE_CODE);
}
