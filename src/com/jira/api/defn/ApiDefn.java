package com.jira.api.defn;

import java.util.*;
import static com.jira.api.defn.ApiDefnHelper.*;
import com.jira.api.config.APIConfig;

public class ApiDefn {

	public static Map<String, APIConfig> apiDefn = new HashMap<>();

	public static void init() {
		initIssuePointSum();
	}

	private static void initIssuePointSum() {
		$api("sumIssuePoint", "issue/sum")//
				.$params(//
						$p($strCol("query", 1, 500), true), //
						$p($strCol("name", 1, 100), true)//
		);//

	}

}
