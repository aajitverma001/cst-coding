package com.jira.api.defn;

import com.jira.api.column.Column;
import com.jira.api.column.StringCol;
import com.jira.api.config.APIConfig;
import com.jira.api.config.APIInputParam;

public class ApiDefnHelper {

	public static APIConfig $api(String methodName, String path) {
		APIConfig conf = new APIConfig(path, methodName);
		ApiDefn.apiDefn.put(normalizePath(path), conf);
		return conf;
	}

	public static APIInputParam $p(Column c, boolean isRequired) {
		APIInputParam p = new APIInputParam();
		p.$col(c).$paramName(c.getColName()).$required(isRequired);
		return p;
	}

	public static APIInputParam $p(Column c) {
		return $p(c, false);
	}

	public static StringCol $strCol(String colName, Integer minChar, Integer maxChar) {
		return new StringCol(colName, minChar, maxChar);
	}
	
	public static String normalizePath(String path){
		return path.replaceAll("/", "_");
	}

}
