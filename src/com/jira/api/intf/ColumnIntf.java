package com.jira.api.intf;

public interface ColumnIntf<U> {

	public void validate(String colName, U colVal);

}
