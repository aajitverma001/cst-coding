package com.jira.api.helper;

import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import com.jira.environment.Env;
import com.jira.error.ApiException;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.mashape.unirest.request.GetRequest;

public class JiraIssuePointSumHelper {

	private final String query;
	private final String name;
	private long pointSum;

	public JiraIssuePointSumHelper(String query, String name) {
		super();
		this.query = query;
		this.name = name;
	}

	/**
	 * NOTE::This processing can be made Async.
	 * @param start
	 */
	public void calculate(int start) {
		JSONObject resp = getPoints(start);
		calculatePoint(resp);
		System.out.println(resp);
		if (!resp.optBoolean("isLast", true)) {
			calculate(resp.getInt("startAt"));
		}
	}

	private void calculatePoint(JSONObject resp) {
		JSONArray arr = resp.optJSONArray("issues");
		if (arr == null) {
			return;
		}
		for (int i = 0; i < arr.length(); i++) {
			JSONObject issue = arr.getJSONObject(i);
			JSONObject fields = issue.getJSONObject("fields");
			pointSum += fields.getInt("storyPoints");
		}
	}

	private JSONObject getPoints(int start) {
		try {
			GetRequest req = Unirest.get(Env.jiraURL() +"/rest/api/2/search");
			req.queryString("q", query).queryString("startAt", start);
			req.header("Accept", "application/json").header("Bearer", Env.prop("jira.auth"));
			HttpResponse<JsonNode> response;
			response = req.asJson();
			JSONObject obj = response.getBody().getObject();
			return obj;
		} catch (UnirestException e) {
			throw new ApiException(HttpServletResponse.SC_SERVICE_UNAVAILABLE, "The Jira Api is down");
		}
	}

	public String sqsMessage() {
		return sqsMessageAsJSON().toString();
	}

	public JSONObject sqsMessageAsJSON() {
		JSONObject resp = new JSONObject();
		resp.put("name", name);
		resp.put("totalPoints", pointSum);
		return resp;
	}

}
