package com.jira.api.column;

import javax.servlet.http.HttpServletResponse;

import com.jira.api.intf.ColumnIntf;
import com.jira.error.ApiException;

public abstract class Column<T, U extends Column<T, ?>> implements ColumnIntf<T> {

	protected String colName;

	public Column(String colName) {
		this.colName = colName;
	}
	
	public String getColName() {
		return colName;
	}

	public T convFromApiClient(String val, String paramName) {
		return convFromApiClient(val, paramName, true);
	}

	public T convFromApiClient(String val, String paramName, boolean validate) {
		T convVal;
		try {
			convVal = _conv(val, paramName);
		} catch (ApiException ex) {
			throw ex;
		} catch (Exception ex) {
			throw new ApiException(HttpServletResponse.SC_BAD_REQUEST, paramName + " - Invalid Format param");
		}
		if (validate) {
			validate(paramName, convVal);
		}
		return convVal;
	}

	@Override
	public void validate(String colName, T colVal) {
		
	}

	public abstract T _conv(String val, String paramName) throws Exception;

}
