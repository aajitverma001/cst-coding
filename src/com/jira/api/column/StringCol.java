package com.jira.api.column;

import javax.servlet.http.HttpServletResponse;

import com.jira.error.ApiException;

public class StringCol extends Column<String, StringCol> {

	private Integer minChar;
	private Integer maxChar;

	public StringCol(String colName, Integer minChar, Integer maxChar) {
		super(colName);
		this.minChar = minChar;
		this.maxChar = maxChar;
	}

	@Override
	public String _conv(String val, String paramName) throws Exception {
		String v = val.trim();
		return v;
	}

	@Override
	public void validate(String colName, String colVal) {
		if (minChar != null && colVal.length() < minChar) {
			throw new ApiException(HttpServletResponse.SC_BAD_REQUEST,
					String.format("%s - Size should be more than %s", colName, minChar));
		}
		if (maxChar != null && colVal.length() > maxChar) {
			throw new ApiException(HttpServletResponse.SC_BAD_REQUEST,
					String.format("%s - Size should not be more than %s", colName, maxChar));
		}
	}

}
