package com.jira.api.endpoint;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;

import org.json.JSONObject;

import com.jira.api.config.APIConfig;
import com.jira.api.helper.JiraIssuePointSumHelper;
import com.jira.error.ApiException;
import com.jira.sqs.QueueDefn;
import com.sqs.codec.impl.SimpleMessageCodec;

@Path("/issue/sum")
public class SumJiraIssuePoint {
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String get(@QueryParam("query") String query, @QueryParam("name") String name, @Context UriInfo uriInfo) {
		JSONObject res = new JSONObject();
		try {
			APIConfig.validateApi(uriInfo);
			JiraIssuePointSumHelper h = new JiraIssuePointSumHelper(query, name);
			h.calculate(0);
			QueueDefn.JIRA_POINT_SUM.sendMessage(new SimpleMessageCodec.Encoder().withMessage(h.sqsMessage()));
			res.put("sqs_message", h.sqsMessageAsJSON());
		} catch (Exception ex) {
			throw ApiException.wrap(ex);
		}
		return res.toString();
	}
}
