package com.jira.api.config;

import com.jira.api.column.Column;

public class APIInputParam {

	private String paramName;
	private Column col;
	private boolean isRequired;

	public APIInputParam $paramName(String paramName) {
		this.paramName = paramName;
		return this;
	}

	public APIInputParam $col(Column col) {
		this.col = col;
		return this;
	}

	public APIInputParam $required(boolean isRequired) {
		this.isRequired = isRequired;
		return this;
	}

	public String getParamName() {
		return paramName;
	}

	public Column getCol() {
		return col;
	}

	public boolean isRequired() {
		return isRequired;
	}

}
