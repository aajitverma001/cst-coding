package com.jira.api.config;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriInfo;

import com.jira.api.defn.ApiDefn;
import com.jira.api.defn.ApiDefnHelper;
import com.jira.error.ApiException;

public class APIConfig {

	private String path;
	private String methodName;
	private List<APIInputParam> params = new ArrayList<>();

	public APIConfig(String path, String methodName) {
		this.path = path;
		this.methodName = methodName;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	public APIConfig $params(APIInputParam... params) {
		this.params.addAll(Arrays.asList(params));
		return this;
	}

	public String getPath() {
		return path;
	}

	public String getMethodName() {
		return methodName;
	}

	public List<APIInputParam> getParams() {
		return params;
	}

	public void validateParam(MultivaluedMap<String, String> params) {
		for (APIInputParam p : this.params) {
			String val = params.getFirst(p.getParamName());//ToDo: Multi-value
			if (p.isRequired() && val == null) {
				throw new ApiException(HttpServletResponse.SC_BAD_REQUEST,
						String.format("%s - cannot be blank", p.getParamName()));
			}
			p.getCol().convFromApiClient(val, p.getParamName());
		}
	}

	public static void validateApi(UriInfo uriInfo) {
		MultivaluedMap<String, String> m = uriInfo.getQueryParameters();
		String path = uriInfo.getPath();
		ApiDefn.apiDefn.get(ApiDefnHelper.normalizePath(path)).validateParam(m);
	}

}
