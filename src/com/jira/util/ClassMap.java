package com.jira.util;

import java.util.Collection;
import java.util.IdentityHashMap;
import java.util.Map;
import java.util.Set;
import java.util.function.BiPredicate;

public class ClassMap<V> implements Map<Class<?>, V> {

    private final Map<Class<?>, V> store;
    private final BiPredicate<Class<?>, Class<?>> strategy;

    private ClassMap(BiPredicate<Class<?>, Class<?>> strategy) {
        this.store = new IdentityHashMap<>();
        this.strategy = strategy;
    }

    public static <V> ClassMap<V> ofSubType() {
        return new ClassMap<>((Class<?> t, Class<?> u) -> u.isAssignableFrom(t));
    }

    public static <V> ClassMap<V> ofSuperType() {
        return new ClassMap<>((Class<?> t, Class<?> u) -> t.isAssignableFrom(u));
    }

    @Override
    public int size() {
        return store.size();
    }

    @Override
    public boolean isEmpty() {
        return store.isEmpty();
    }

    @Override
    public boolean containsKey(Object key) {
        return getEntry((Class<?>) key) != null;
    }

    @Override
    public boolean containsValue(Object value) {
        return store.containsValue(value);
    }

    @Override
    public V get(Object key) {
        Map.Entry<Class<?>, V> entry = getEntry((Class<?>) key);
        if (entry == null) {
            return null;
        }
        return entry.getValue();
    }

    @Override
    public V put(Class<?> key, V value) {
        return store.put(key, value);
    }

    @Override
    public V remove(Object key) {
        return store.remove(key);
    }

    @Override
    public void putAll(Map<? extends Class<?>, ? extends V> map) {
        store.putAll(map);
    }

    @Override
    public void clear() {
        store.clear();
    }

    @Override
    public Set<Class<?>> keySet() {
        return store.keySet();
    }

    @Override
    public Collection<V> values() {
        return store.values();
    }

    @Override
    public Set<Entry<Class<?>, V>> entrySet() {
        return store.entrySet();
    }

    private Map.Entry<Class<?>, V> getEntry(Class<?> type) {
        for (Map.Entry<Class<?>, V> entry : store.entrySet()) {
            if (entry.getKey().equals(type)) {
                return entry;
            }
        }
        for (Map.Entry<Class<?>, V> entry : store.entrySet()) {
            if (strategy.test(entry.getKey(), type)) {
                return entry;
            }
        }
        return null;
    }
}