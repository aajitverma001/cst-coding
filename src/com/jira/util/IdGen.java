package com.jira.util;

import java.util.concurrent.atomic.AtomicInteger;


public class IdGen {
	private static final String baseDigits = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

	// Server start time.
	private static final String fixedPrefix = getFixedPrefix();

	private static AtomicInteger id = new AtomicInteger(0);

	public static String nextId() {
		String time = conv(System.currentTimeMillis());
		return fixedPrefix + time + conv(id.incrementAndGet());
	}

	private static String conv(long val) {
		StringBuilder tempVal = new StringBuilder(20);
		if (val == 0) {
			tempVal.append('0');
		}
		int mod = 0;
		while (val != 0) {
			mod = (int) (val % baseDigits.length());
			tempVal.append(baseDigits.charAt(mod));
			val = val / baseDigits.length();
		}
		return tempVal.toString();
	}

	private static String getFixedPrefix() {
		return conv(System.currentTimeMillis() % 1000);
	}
	
	public static void main(String[] args) {
		System.out.println(IdGen.nextId());
	}

}
