package com.jira.container;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.http.HttpServletResponse;

import com.jira.api.defn.ApiDefn;
import com.jira.environment.Env;
import com.jira.error.ServiceFailureException;
import com.jira.sqs.QueueDefn;
import com.sqs.client.initializer.AwsQueueInitializer;

public class InitServlet implements ServletContextListener {

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {

	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		Env.init();
		ApiDefn.init();
		try {
			AwsQueueInitializer.init(QueueDefn.class);
		} catch (Exception e) {
			e.printStackTrace();
			throw new ServiceFailureException(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
					"SQS Initialization failed!!", e.getCause());
		}
		System.out.println("******************    Initializatin complete ******************");
	}
}
