package com.sqs.client.impl;

import java.util.*;

import com.amazonaws.AmazonWebServiceRequest;
import com.amazonaws.services.sqs.model.*;
import com.amazonaws.services.sqs.*;
import com.sqs.client.initializer.AwsMessageCodec;
import com.sqs.client.initializer.AwsQueueConfig;
import com.sqs.client.intf.AwsQueueClient;
import com.sqs.codec.intf.AwsMessageDecoder;
import com.sqs.codec.intf.AwsMessageEncoder;

public class AwsGeneralQueueClient implements AwsQueueClient.GeneralQueue {

    private final String queueUrl;
    private final AwsQueueConfig config;
    private final AmazonSQS client;

    public AwsGeneralQueueClient(AwsQueueConfig config, AmazonSQS client) {
        this.queueUrl = config.getQueueUrl();
        this.config = config;
        this.client = client;
    }

    @Override
    public AwsQueueConfig getConfig() {
        return config;
    }

    @Override
    public SendMessageResult sendMessage(AwsMessageEncoder encodedMessage) throws Exception {
        SendMessageRequest smr = updateReq(encodedMessage.encode().clone());
        smr.setQueueUrl(queueUrl);
        Map<String, MessageAttributeValue> map = smr.getMessageAttributes() != null ? smr.getMessageAttributes() : new HashMap<>();
        map.put(MessageAttrs.TYPE_ATTR, new MessageAttributeValue().withDataType("String").withStringValue(encodedMessage.typeCode()));
        map.put(MessageAttrs.QUEUE_INFO_ATTR, new MessageAttributeValue().withDataType("String").withStringValue(getQueueInfo(config)));
        smr.setDelaySeconds(config.getDelaySecs());
        smr.setMessageAttributes(map);
        return client.sendMessage(smr);
    }

    @Override
    public SendMessageBatchResult sendMessageBatch(AwsMessageEncoder... encodedMessages) throws Exception {
        SendMessageBatchRequest smbr = updateReq(new SendMessageBatchRequest());
        smbr.setQueueUrl(queueUrl);
        List<SendMessageBatchRequestEntry> entries = new ArrayList<>();
        for (AwsMessageEncoder encodedMessage : encodedMessages) {
            SendMessageBatchRequestEntry entry = encodedMessage.encodeEntry();
            Map<String, MessageAttributeValue> map = entry.getMessageAttributes() != null ? entry.getMessageAttributes() : new HashMap<>();
            map.put(MessageAttrs.TYPE_ATTR, new MessageAttributeValue().withDataType("String").withStringValue(encodedMessage.typeCode()));
            map.put(MessageAttrs.QUEUE_INFO_ATTR, new MessageAttributeValue().withDataType("String").withStringValue(getQueueInfo(config)));
            entry.setDelaySeconds(config.getDelaySecs());
            entry.setMessageAttributes(map);
            entries.add(entry);
        }
        smbr.setEntries(entries);
        return client.sendMessageBatch(smbr);
    }

    @Override
    public List<? extends AwsMessageDecoder> receiveMessage(ReceiveMessageRequest rmr) throws Exception {
        rmr = updateReq(rmr.clone());
        rmr.setQueueUrl(queueUrl);
        rmr.setMaxNumberOfMessages(config.getMaxMessageCount());
        rmr.setWaitTimeSeconds(config.getWaitTimeSecs());
        if (config.getVisibilityTimeoutSecs() != -1) {
            rmr.setVisibilityTimeout(config.getVisibilityTimeoutSecs());
        }
        ReceiveMessageResult result = client.receiveMessage(rmr);
        List<AwsMessageDecoder> decodedMessages = new ArrayList<>();
        for (Message message : result.getMessages()) {
            decodedMessages.add(decode(message));
        }
        return Collections.unmodifiableList(decodedMessages);
    }

    @Override
    public DeleteMessageResult deleteMessage(DeleteMessageRequest dmr) throws Exception {
        dmr = updateReq(dmr.clone());
        dmr.setQueueUrl(queueUrl);
        return client.deleteMessage(dmr);
    }

    @Override
    public DeleteMessageBatchResult deleteMessageBatch(DeleteMessageBatchRequest dmbr) throws Exception {
        dmbr = updateReq(dmbr.clone());
        dmbr.setQueueUrl(queueUrl);
        return client.deleteMessageBatch(dmbr);
    }

    @Override
    public ChangeMessageVisibilityResult changeMessageVisibility(ChangeMessageVisibilityRequest cmvr) throws Exception {
        cmvr = updateReq(cmvr.clone());
        cmvr.setQueueUrl(queueUrl);
        return client.changeMessageVisibility(cmvr);
    }

    @Override
    public ChangeMessageVisibilityBatchResult changeMessageVisibilityBatch(ChangeMessageVisibilityBatchRequest cmvbr) throws Exception {
        cmvbr = updateReq(cmvbr.clone());
        cmvbr.setQueueUrl(queueUrl);
        return client.changeMessageVisibilityBatch(cmvbr);
    }

    @Override
    public GetQueueAttributesResult getQueueAttributes(GetQueueAttributesRequest gqar) throws Exception {
        gqar = updateReq(gqar.clone());
        gqar.setQueueUrl(queueUrl);
        return client.getQueueAttributes(gqar);
    }

    @Override
    public ListQueueTagsResult listQueueTags(ListQueueTagsRequest lqtr) throws Exception {
        lqtr = updateReq(lqtr.clone());
        lqtr.setQueueUrl(queueUrl);
        return client.listQueueTags(lqtr);
    }

    @Override
    public AwsMessageDecoder decode(Message message) throws Exception {
        String typeCode = message.getMessageAttributes().get(MessageAttrs.TYPE_ATTR).getStringValue();
        return AwsMessageCodec.getDecoderInst(typeCode).decode(message);
    }

    private <T extends AmazonWebServiceRequest> T updateReq(T req) {
        req.setRequestCredentialsProvider(config.getCredentials());
        return req;
    }

    private static String getQueueInfo(AwsQueueConfig config) {
        return String.format("%s/%s/%s", config.getRegion(), config.getAccountId(), config.getQueueName());
    }
}
