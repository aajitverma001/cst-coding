package com.sqs.client.impl;

import java.io.IOException;
import java.net.Socket;

import com.jira.environment.Env;
/**
 * Mock server configuration bean.
 * @author cb-ajit
 *
 */
public class SqsMockServer {

    public static String hostAddress() {
        return Env.prop("sqs.mock_server.interface", "localhost");
    }

    public static int portAddress() {
        return Env.intProp("sqs.mock_server.port", 1337);
    }

    public static boolean isServerRunning() {
        try (Socket s = new Socket(hostAddress(), portAddress())) {
            return true;
        } catch (IOException ex) {
        }
        return false;
    }

}
