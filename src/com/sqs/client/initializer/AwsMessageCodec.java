package com.sqs.client.initializer;

import java.util.Map;
import java.util.AbstractMap.*;

import com.sqs.codec.intf.AwsMessageDecoder;
import com.sqs.codec.intf.AwsMessageEncoder;

import java.util.HashMap;

/**
 * Queue specific Codec holder.
 * 
 * @author cb-ajit
 *
 */
public class AwsMessageCodec {

	private static final Map<String, Map.Entry<Class<? extends AwsMessageEncoder>, Class<? extends AwsMessageDecoder>>> CODECS = new HashMap<>();

	public static void registerCodec(String typeCode, Class<? extends AwsMessageEncoder> encoder,
			Class<? extends AwsMessageDecoder> decoder) {
		CODECS.put(typeCode, new SimpleImmutableEntry<>(encoder, decoder));
	}

	public static Class<? extends AwsMessageEncoder> getEncoderType(String typeCode) {
		return CODECS.get(typeCode).getKey();
	}

	public static Class<? extends AwsMessageDecoder> getDecoderType(String typeCode) {
		return CODECS.get(typeCode).getValue();
	}

	public static AwsMessageEncoder getEncoderInst(String typeCode) {
		try {
			return getEncoderType(typeCode).newInstance();
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	public static AwsMessageDecoder getDecoderInst(String typeCode) {
		try {
			return getDecoderType(typeCode).newInstance();
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

}
