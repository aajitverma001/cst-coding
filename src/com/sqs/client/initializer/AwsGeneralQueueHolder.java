package com.sqs.client.initializer;

import com.amazonaws.services.sqs.model.*;
import com.sqs.client.intf.AwsQueueClient;
import com.sqs.codec.intf.AwsMessageDecoder;
import com.sqs.codec.intf.AwsMessageEncoder;

import java.util.*;

/**
 * General type queue holder.
 * 
 * @author cb-ajit
 *
 */
class AwsGeneralQueueHolder extends AwsQueueHolder implements AwsQueueClient.GeneralQueue {

	AwsGeneralQueueHolder(String queueType, String typeCode) {
		super(queueType, typeCode);
	}

	@Override
	public SendMessageResult sendMessage(AwsMessageEncoder encodedMessage) throws Exception {
		assertEncoderType(encodedMessage);
		return client().sendMessage(encodedMessage);
	}

	@Override
	public SendMessageBatchResult sendMessageBatch(AwsMessageEncoder... encodedMessages) throws Exception {
		for (AwsMessageEncoder encodedMessage : encodedMessages) {
			assertEncoderType(encodedMessage);
		}
		return client().sendMessageBatch(encodedMessages);
	}

	@Override
	public List<? extends AwsMessageDecoder> receiveMessage(ReceiveMessageRequest rmr) throws Exception {
		List<? extends AwsMessageDecoder> decodedMessages = client().receiveMessage(rmr);
		for (AwsMessageDecoder decodedMessage : decodedMessages) {
			assertDecoderType(decodedMessage);
		}
		return decodedMessages;
	}

	@Override
	public DeleteMessageResult deleteMessage(DeleteMessageRequest dmr) throws Exception {
		return client().deleteMessage(dmr);
	}

	@Override
	public DeleteMessageBatchResult deleteMessageBatch(DeleteMessageBatchRequest dmbr) throws Exception {
		return client().deleteMessageBatch(dmbr);
	}

	@Override
	public ChangeMessageVisibilityResult changeMessageVisibility(ChangeMessageVisibilityRequest cmvr) throws Exception {
		return client().changeMessageVisibility(cmvr);
	}

	@Override
	public ChangeMessageVisibilityBatchResult changeMessageVisibilityBatch(ChangeMessageVisibilityBatchRequest cmvbr)
			throws Exception {
		return client().changeMessageVisibilityBatch(cmvbr);
	}

	@Override
	public GetQueueAttributesResult getQueueAttributes(GetQueueAttributesRequest gqar) throws Exception {
		return client().getQueueAttributes(gqar);
	}

	@Override
	public ListQueueTagsResult listQueueTags(ListQueueTagsRequest lqtr) throws Exception {
		return client().listQueueTags(lqtr);
	}

	@Override
	public AwsMessageDecoder decode(Message message) throws Exception {
		AwsMessageDecoder decoder = client().decode(message);
		assertDecoderType(decoder);
		return decoder;
	}

	private AwsQueueClient.GeneralQueue client() {
		return getClient(AwsQueueClient.GeneralQueue.class);
	}

}
