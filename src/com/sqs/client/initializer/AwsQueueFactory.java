package com.sqs.client.initializer;

import java.util.Objects;
import java.util.function.Function;

import com.jira.util.ClassMap;
import com.sqs.client.intf.AwsQueueClient;

/**
 * Queue registry to hold registered queues.
 * 
 * @author cb-ajit
 *
 */
public class AwsQueueFactory {

	private static final ClassMap<Function<Object[], AwsQueueClient>> CLIENTS = ClassMap.ofSubType();

	public static <T extends AwsQueueClient> T get(Class<T> type, Object... args) {
		return get(type, null, args);
	}

	public static <T extends AwsQueueClient> T get(Class<T> type, Function<Object[], AwsQueueClient> defaultFactory,
			Object... args) {
		Objects.requireNonNull(type);
		Object client = CLIENTS.containsKey(type) ? CLIENTS.get(type).apply(args) : null;
		if (client != null) {
			return (T) client;
		}
		if (defaultFactory == null) {
			return null;
		}
		return (T) defaultFactory.apply(args);
	}

	public static void register(Class type, Function<Object[], AwsQueueClient> factory) {
		Objects.requireNonNull(type);
		Objects.requireNonNull(factory);
		CLIENTS.put(type, factory);
	}

	public static void deregister(Class type) {
		Objects.requireNonNull(type);
		CLIENTS.remove(type);
	}

	public static void clear() {
		CLIENTS.clear();
	}
}