package com.sqs.client.initializer;

import java.util.function.*;

import com.amazonaws.auth.AWSCredentialsProvider;

/**
 * AWS queue configuration;
 * 
 * @author cb-ajit
 *
 */
public final class AwsQueueConfig {

	private Store setter = new Store();
	private Store getter;

	private static class Store {

		private String accountId;
		private String region = "us-east-1";
		private String queueName;
		private int maxMessageCount = 10;
		private int waitTimeSecs = 0;
		private int delaySecs = 0;
		private AWSCredentialsProvider creds;
		private int visibilityTimeoutSecs = -1;
		private Function<AwsQueueConfig, String> urlProvider;
	}

	public AwsQueueConfig() {

	}

	private AwsQueueConfig(AwsQueueConfig config) {
		setter.accountId = config.getAccountId();
		setter.region = config.getRegion();
		setter.queueName = config.getQueueName();
		setter.creds = config.getCredentials();
		setter.maxMessageCount = config.getMaxMessageCount();
		setter.waitTimeSecs = config.getWaitTimeSecs();
		setter.delaySecs = config.getDelaySecs();
		setter.visibilityTimeoutSecs = config.getVisibilityTimeoutSecs();
		setter.urlProvider = config.getter.urlProvider;
	}

	public AwsQueueConfig setAccountId(String accountId) {
		setter.accountId = accountId;
		return this;
	}

	public AwsQueueConfig setRegion(String region) {
		setter.region = region;
		return this;
	}

	public AwsQueueConfig setQueueName(String queueName) {
		setter.queueName = queueName;
		return this;
	}

	public AwsQueueConfig setCredentials(AWSCredentialsProvider creds) {
		setter.creds = creds;
		return this;
	}

	public AwsQueueConfig setMaxMessageCount(int maxMessageCount) {
		setter.maxMessageCount = maxMessageCount;
		return this;
	}

	public AwsQueueConfig setWaitTimeSecs(int waitTimeSecs) {
		setter.waitTimeSecs = waitTimeSecs;
		return this;
	}

	public AwsQueueConfig setDelaySecs(int delaySecs) {
		setter.delaySecs = delaySecs;
		return this;
	}

	public AwsQueueConfig setVisibilityTimeoutSecs(int visibilityTimeoutSecs) {
		setter.visibilityTimeoutSecs = visibilityTimeoutSecs;
		return this;
	}

	public AwsQueueConfig setUrlProvider(Function<AwsQueueConfig, String> urlProvider) {
		setter.urlProvider = urlProvider;
		return this;
	}

	public String getAccountId() {
		return getter.accountId;
	}

	public String getRegion() {
		return getter.region;
	}

	public String getQueueName() {
		return getter.queueName;
	}

	public AWSCredentialsProvider getCredentials() {
		return getter.creds;
	}

	public int getMaxMessageCount() {
		return getter.maxMessageCount;
	}

	public int getWaitTimeSecs() {
		return getter.waitTimeSecs;
	}

	public int getDelaySecs() {
		return getter.delaySecs;
	}

	public int getVisibilityTimeoutSecs() {
		return getter.visibilityTimeoutSecs;
	}

	public String getQueueUrl() {
		return getter.urlProvider.apply(this);
	}

	@Override
	public String toString() {
		return getAccountId() + "/" + getRegion() + "/" + getQueueName();
	}

	public AwsQueueConfig close() {
		if (getter != null) {
			return this;
		}
		this.getter = setter;
		this.setter = null;
		return this;
	}

	public AwsQueueConfig copy() {
		if (getter == null) {
			return this;
		}
		return new AwsQueueConfig(this);
	}
}