package com.sqs.client.initializer;

import java.io.File;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.CreateQueueRequest;
import com.amazonaws.services.sqs.model.CreateQueueResult;
import com.jira.environment.Env;
import com.sqs.client.impl.AwsGeneralQueueClient;
import com.sqs.client.impl.SqsMockServer;
import com.sqs.client.intf.AwsQueueClient;

/**
 * Queue Initialization utilities.
 * For Production configuration are picked from s3.
 * @author cb-ajit
 *
 */
public class AwsQueueInitializer {

	private static boolean initialized = false;
	private static JSONObject queueConfig;
	private static AmazonSQS mainClient;

	public static void init(Class... queueClasses) throws Exception {
		assertNotInitialized();
		String accountId = Env.reqStrProp("aws.sqs.account_id");
		String region = Env.prop("aws.sqs.region", "us-east-1");
		AWSCredentialsProvider creds = getCredentials();
		queueConfig = loadConfig();

		mainClient = loadMainClient(creds, region);
		registerMocks();// Uses mock (elastic.mq) in DEV mode always. 

		for (Class queueClass : queueClasses) {
			for (Field field : queueClass.getFields()) {
				Object type = field.get(null);
				if (!AwsQueueHolder.class.isInstance(type)) {
					continue;
				}

				AwsQueueHolder holder = AwsQueueHolder.class.cast(type);
				List<AwsQueueClient.GeneralQueue> clients = getQueuesFromConfig(accountId, region, creds, holder);

				if (AwsGeneralQueueHolder.class.isInstance(type)) {
					holder.setClient(clients.get(0));
				}
			}
		}
		initialized = true;
	}

	public static AwsQueueClient.GeneralQueue instQ(String queueType, String typeCode) {
		assertNotInitialized();
		return new AwsGeneralQueueHolder(queueType, typeCode);
	}

	public static AmazonSQS _getMainClient() {
		assertInitialized();
		return mainClient;
	}

	public static void shutdown() {
		assertInitialized();
		mainClient.shutdown();
	}

	private static void assertNotInitialized() {
		assert(!initialized);
	}

	private static void assertInitialized() {
		assert(initialized);
	}

	private static List<AwsQueueClient.GeneralQueue> getQueuesFromConfig(String accountId, String region,
			AWSCredentialsProvider creds, AwsQueueHolder holder) {
		if (!queueConfig.has(holder.getQueueType())) {
			throw new ExceptionInInitializerError(
					"Queue config for the type '" + holder.getQueueType() + "' not found");
		}
		try {
			List<AwsQueueClient.GeneralQueue> clients = new ArrayList<>();
			JSONArray queues = queueConfig.getJSONArray(holder.getQueueType());
			if (queues.length() == 0) {
				throw new ExceptionInInitializerError("No queue defined for the type '" + holder.getQueueType() + "'");
			}
			for (int i = 0; i < queues.length(); i++) {
				AwsQueueConfig config = new AwsQueueConfig();
				config.setAccountId(accountId);
				config.setRegion(region);
				config.setCredentials(creds);
				fillConfig(config, queues.getJSONObject(i));
				config.setUrlProvider((c) -> queueUrl(c.getAccountId(), c.getRegion(), c.getQueueName()));
				config.close();
				clients.add(AwsQueueFactory.get(AwsQueueClient.GeneralQueue.class,
						(args) -> new AwsGeneralQueueClient((AwsQueueConfig) args[0], (AmazonSQS) args[1]), config,
						mainClient, holder));
			}
			return clients;
		} catch (JSONException ex) {
			throw new ExceptionInInitializerError(ex);
		}
	}

	public static String queueUrl(String accountId, String region, String queueName) {
		return String.format("https://sqs.%s.amazonaws.com/%s/%s", region, accountId, queueName);
	}

	private interface QueueKeys {

		String ACCOUNT_ID = "account_id";
		String QUEUE_NAME = "queue_name";
		String REGION = "region";
		String DELAY_SECS = "delay_secs";
		String MAX_MESSAGE_COUNT = "max_message_count";
		String VISIBILITY_TIMEOUT = "visibility_timeout_secs";
		String WAIT_TIME = "wait_time_secs";
	}

	private static AwsQueueConfig fillConfig(AwsQueueConfig queueConfig, JSONObject queue) throws JSONException {
		String queuePrefix = Env.prop("aws.sqs.queue_name_prefix", "");
		queueConfig.setQueueName(
				queuePrefix + (!queuePrefix.isEmpty() ? "-" : "") + queue.getString(QueueKeys.QUEUE_NAME));
		if (queue.has(QueueKeys.ACCOUNT_ID)) {
			queueConfig.setAccountId(queue.getString(QueueKeys.ACCOUNT_ID));
		}
		if (queue.has(QueueKeys.REGION)) {
			queueConfig.setRegion(queue.getString(QueueKeys.REGION));
		}
		if (queue.has(QueueKeys.DELAY_SECS)) {
			queueConfig.setDelaySecs(queue.getInt(QueueKeys.DELAY_SECS));
		}
		if (queue.has(QueueKeys.MAX_MESSAGE_COUNT)) {
			queueConfig.setMaxMessageCount(queue.getInt(QueueKeys.MAX_MESSAGE_COUNT));
		}
		if (queue.has(QueueKeys.VISIBILITY_TIMEOUT)) {
			queueConfig.setVisibilityTimeoutSecs(queue.getInt(QueueKeys.VISIBILITY_TIMEOUT));
		}
		if (queue.has(QueueKeys.WAIT_TIME)) {
			queueConfig.setWaitTimeSecs(queue.getInt(QueueKeys.WAIT_TIME));
		}
		return queueConfig;
	}

	private static AWSCredentialsProvider getCredentials() {
		return new AWSStaticCredentialsProvider(new BasicAWSCredentials(Env.reqStrProp("aws.sqs.accesskey"),
				Env.reqStrProp("aws.sqs.secretkey")));
	}

	private static AmazonSQS loadMainClient(AWSCredentialsProvider creds, String region) {
		if (isMockEnabled()) {
			return AmazonSQSClientBuilder.standard().withCredentials(creds)
					.withEndpointConfiguration(new AwsClientBuilder.EndpointConfiguration(
							"http://" + SqsMockServer.hostAddress() + ":" + SqsMockServer.portAddress(), "mockregion"))
					.build();
		}

		return AmazonSQSClientBuilder.standard().withCredentials(creds).withRegion(region).build();
	}

	private static JSONObject loadConfig() {
		try {
			if (Env.isDev()) {
				return loadFromLocal();
			}
			return loadFromS3();
		} catch (Exception ex) {
			throw new ExceptionInInitializerError(ex);
		}
	}

	private static JSONObject loadFromS3() throws Exception {
		// load from s3
		return new JSONObject();
	}

	private static <T> JSONObject loadFromLocal() throws Exception {
		JSONObject queueJson = new JSONObject();
		JSONObject confJson = new JSONObject(FileUtils.readFileToString(new File(Env.baseDir() + "sqs_conf.json")));
		for (Iterator it = confJson.keys(); it.hasNext();) {
			String key = (String) it.next();
			assert(!queueJson.has(key));
			JSONObject confQueueType = confJson.getJSONObject(key);
			JSONArray confQueueArr = confQueueType.getJSONArray("queues");
			JSONArray queueJsonArr = new JSONArray();
			for (int i = 0; i < confQueueArr.length(); i++) {
				JSONObject queueOut = new JSONObject();
				String queueName = confQueueArr.getString(i);
				queueOut.put(QueueKeys.QUEUE_NAME, queueName);
				queueOut.toMap().putAll(confQueueType.toMap());
				queueOut.toMap().remove("queues");
				queueJsonArr.put(queueOut);
			}
			queueJson.put(key, queueJsonArr);
		}
		return queueJson;
	}

	public static boolean isMockEnabled() {
		return Env.isDev();
	}

	private static void registerMocks() throws Exception {
		if (isMockEnabled()) {
			AwsQueueFactory.register(AwsQueueClient.GeneralQueue.class, (Object[] t) -> {
				AwsQueueConfig config = (AwsQueueConfig) t[0];
				AmazonSQS client = mainClient;
				Map<String, String> queueAttrs = new HashMap<>();
				queueAttrs.put("DelaySeconds", "" + config.getDelaySecs());
				queueAttrs.put("MessageRetentionPeriod", "1209600");// 14 days
				queueAttrs.put("ReceiveMessageWaitTimeSeconds", "" + config.getWaitTimeSecs());
				if (config.getVisibilityTimeoutSecs() != -1) {
					queueAttrs.put("VisibilityTimeout", "" + config.getVisibilityTimeoutSecs());
				}
				CreateQueueResult result = client.createQueue(
						new CreateQueueRequest().withQueueName(config.getQueueName()).withAttributes(queueAttrs));
				config = config.copy();
				config.setUrlProvider((c) -> result.getQueueUrl());
				config.close();
				return new AwsGeneralQueueClient(config, client);
			});
		}
	}

}
