package com.sqs.client.initializer;

import com.sqs.client.intf.AwsQueueClient;
import com.sqs.codec.intf.AwsMessageDecoder;
import com.sqs.codec.intf.AwsMessageEncoder;

public class AwsQueueHolder implements AwsQueueClient {

	private final String queueType;
	private final String typeCode;
	private AwsQueueClient client;

	AwsQueueHolder(String queueType, String typeCode) {
		this.queueType = queueType;
		this.typeCode = typeCode;
	}

	String getQueueType() {
		return queueType;
	}

	Class<? extends AwsMessageEncoder> getMessageEncoder() {
		return AwsMessageCodec.getEncoderType(typeCode);
	}

	Class<? extends AwsMessageDecoder> getMessageDecoder() {
		return AwsMessageCodec.getDecoderType(typeCode);
	}

	<T extends AwsQueueClient> T getClient(Class<T> type) {
		return type.cast(client);
	}

	void setClient(AwsQueueClient client) {
		this.client = client;
	}

	void assertEncoderType(AwsMessageEncoder encoder) {
		if (!getMessageEncoder().equals(encoder.getClass())) {
			throw new ClassCastException("Passed encoder '" + encoder.getClass() + "'. Expected encoder '"
					+ getMessageEncoder().getName() + "'");
		}
	}

	void assertDecoderType(AwsMessageDecoder decoder) {
		if (!getMessageDecoder().equals(decoder.getClass())) {
			throw new ClassCastException("Passed decoder '" + decoder.getClass() + "'. Expected decoder '"
					+ getMessageDecoder().getName() + "'");
		}
	}

	@Override
	public AwsQueueConfig getConfig() {
		return client.getConfig();
	}
}
