package com.sqs.client.intf;

import com.amazonaws.services.sqs.model.*;
import com.sqs.client.initializer.AwsQueueConfig;
import com.sqs.codec.intf.AwsMessageDecoder;
import com.sqs.codec.intf.AwsMessageEncoder;

import java.util.*;

/**
 * Contract for the Queue client
 * 
 * @author cb-ajit
 *
 */
public interface AwsQueueClient {

	AwsQueueConfig getConfig();

	interface MessageAttrs {

		String TYPE_ATTR = "__type__";
		String QUEUE_INFO_ATTR = "__queue__";
	}

	/**
	 * To support SQS client where its task is only to send message
	 * 
	 * @param <T>
	 */
	interface PushQueue<T extends AwsMessageEncoder> extends AwsQueueClient {

		SendMessageResult sendMessage(T encodedMessage) throws Exception;

		SendMessageBatchResult sendMessageBatch(T... encodedMessages) throws Exception;
	}

	/**
	 * To support SQS client where its task is only to consume message
	 * 
	 * @param <T>
	 */
	interface PullQueue<T extends AwsMessageDecoder> extends AwsQueueClient {

		T decode(Message message) throws Exception;

		List<? extends T> receiveMessage(ReceiveMessageRequest rmr) throws Exception;

	}

	/**
	 * To support SQS client where its task is only to delete message
	 * 
	 */
	interface DeleteQueue extends AwsQueueClient {

		DeleteMessageResult deleteMessage(DeleteMessageRequest dmr) throws Exception;

		DeleteMessageBatchResult deleteMessageBatch(DeleteMessageBatchRequest dmbr) throws Exception;
	}

	/**
	 * To support SQS client where it can read and push message.
	 *
	 * @param <T>
	 * @param <V>
	 */
	interface GeneralQueue<T extends AwsMessageEncoder, V extends AwsMessageDecoder>
			extends PushQueue<T>, PullQueue<V>, DeleteQueue {

		ChangeMessageVisibilityResult changeMessageVisibility(ChangeMessageVisibilityRequest cmvr) throws Exception;

		ChangeMessageVisibilityBatchResult changeMessageVisibilityBatch(ChangeMessageVisibilityBatchRequest cmvbr)
				throws Exception;

		GetQueueAttributesResult getQueueAttributes(GetQueueAttributesRequest gqar) throws Exception;

		ListQueueTagsResult listQueueTags(ListQueueTagsRequest lqtr) throws Exception;

	}

	/**
	 * To support SQS client where it can read and push message to cluster for
	 * Queues.
	 *
	 * @param <T>
	 * @param <V>
	 */

	interface MultiQueue<T extends AwsMessageEncoder> extends PushQueue<T> {

		List<? extends GeneralQueue> getQueueClients();
	}

}
