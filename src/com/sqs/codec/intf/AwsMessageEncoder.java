package com.sqs.codec.intf;

import com.amazonaws.services.sqs.model.SendMessageBatchRequestEntry;
import com.amazonaws.services.sqs.model.SendMessageRequest;

/**
 * Common place where message can be encoded that are to be pushed to the Queue.
 * Ex. Setting tenant information to message Attributes.
 * 
 * @author cb-ajit
 *
 */
public abstract class AwsMessageEncoder {

	protected String messageBody;

	public final String messageBody() {
		return messageBody;
	}

	public abstract String typeCode();

	public abstract SendMessageRequest encode() throws Exception;

	public abstract SendMessageBatchRequestEntry encodeEntry() throws Exception;
}
