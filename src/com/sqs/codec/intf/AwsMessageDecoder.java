package com.sqs.codec.intf;

import com.amazonaws.services.sqs.model.Message;

/**
 * Common place where message can be parsed that are received from the Queue.
 * Ex. Setting tenant information from message Attributes.
 * 
 * @author cb-ajit
 *
 */
public abstract class AwsMessageDecoder {

	protected Message message;

	public AwsMessageDecoder() {
	}

	public final Message message() {
		return message;
	}

	public final String messageBody() {
		return message.getBody();
	}

	public abstract String typeCode();

	public abstract AwsMessageDecoder decode(Message message) throws Exception;
}
