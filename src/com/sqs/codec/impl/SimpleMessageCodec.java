package com.sqs.codec.impl;

import com.amazonaws.services.sqs.model.Message;
import com.jira.util.IdGen;
import com.sqs.client.initializer.AwsMessageCodec;
import com.sqs.codec.intf.AwsMessageDecoder;
import com.sqs.codec.intf.AwsMessageEncoder;
import com.amazonaws.services.sqs.model.*;

/**
 * Simple Codec for this assignment.
 * 
 * @author cb-ajit
 *
 */
public class SimpleMessageCodec {
	public static final String TYPE_CODE;

	static {
		TYPE_CODE = "simple_message";
		AwsMessageCodec.registerCodec(TYPE_CODE, Encoder.class, Decoder.class);
	}

	public static class Encoder extends AwsMessageEncoder {

		private String batchId = IdGen.nextId();

		public Encoder withMessage(String messageBody) {
			this.messageBody = messageBody;
			return this;
		}

		public Encoder withBatchId(String batchId) {
			this.batchId = batchId;
			return this;
		}

		@Override
		public String typeCode() {
			return TYPE_CODE;
		}

		@Override
		public SendMessageRequest encode() {
			return new SendMessageRequest().withMessageBody(messageBody);
		}

		@Override
		public SendMessageBatchRequestEntry encodeEntry() {
			return new SendMessageBatchRequestEntry().withId(batchId).withMessageBody(messageBody);
		}
	}

	public static class Decoder extends AwsMessageDecoder {

		@Override
		public Decoder decode(Message message) {
			this.message = message;
			return this;
		}

		@Override
		public String typeCode() {
			return TYPE_CODE;
		}
	}
}
